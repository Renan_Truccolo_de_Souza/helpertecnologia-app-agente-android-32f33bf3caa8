import React, {Component} from 'react';
import {View, Text, ScrollView, StatusBar } from 'react-native';
import { createAppContainer, SafeAreaView} from 'react-navigation';
import {createDrawerNavigator, DrawerItems} from 'react-navigation-drawer';
import {Container} from 'native-base';
import OpenStreetMapScreen from '../../OpenStreetMapScreen/OpenStreetMap';
import GoogleMapScreen from '../../GoogleMaps/GoogleMapScreen';

const CustomDrawerContentComponent = (props) => (
    <ScrollView>
        <View style={{height: 80, backgroundColor: "#la8cff", alignItems: 'center', justifyContent: 'center'}} >
           <Text style={{color: 'white', fontSize: 30 }}>Maps</Text>
        </View>
           <SafeAreaView style={{flex: 1}} forceInset={{ top: 'always', horizontal: 'never'}}>
               <DrawerItems {...props} />
           </SafeAreaView>
    </ScrollView>
);

const MyDrawerNavigator = createDrawerNavigator(
    {
        GoogleMap:GoogleMapScreen,
    OpenStreetMap:OpenStreetMapScreen,
    },
    {
    contentComponent:CustomDrawerContentComponent
    }
);

const MyApp = createAppContainer(MyDrawerNavigator);

export default class Main extends React.Component{
    static navigationOptions = {
        header: null,
    };

    render() {
           return (
               <Container>
                   <StatusBar hidden/>
                  <MyApp />
               </Container>
           );
    }
}