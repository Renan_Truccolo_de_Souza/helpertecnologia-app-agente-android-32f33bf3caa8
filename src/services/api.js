import axios from 'axios';
import {AsyncStorage} from "react-native";

const api = axios.create({
    baseURL: 'https://app.helpertecnologia.com.br'
});

export async function getHelpers(userLocation) {
    let valid_token = "";
    valid_token = await _retriveData(valid_token);
    return new Promise((resolve, reject) => {

        console.log(valid_token + " token");

        return api.get('/api/helper', {
            headers: {
                "Authorization": `Bearer ${valid_token}`,
            },
        }).then(res => {
                    const helper = res.data;
                    resolve(helper);
                }).catch(error => {
                reject(error);
                })
    });

}

const _retriveData = async (valid_token) => {
    try{
        let value = await AsyncStorage.getItem('@api:token');

        if(value !== null){
            valid_token = value;
            return valid_token;
        }
    }catch (e) {
        console.log(e)
    }
};

export default api;