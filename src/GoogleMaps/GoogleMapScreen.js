import React from 'react';
import { View, StyleSheet, Image, Dimensions, Text,  ScrollView } from 'react-native';
import { Button, Container, Header, Left, Right, Icon} from "native-base";
import MapView, { MAP_TYPES, PROVIDER_DEFAULT, Marker } from "react-native-maps";

import * as Permissions from "expo-permissions"
import * as Location  from "expo-location";

import {getHelpers} from "../services/api";

import * as geolib from "geolib";

import  Modal  from "react-native-modal";

import { createAppContainer, SafeAreaView } from "react-navigation";
import { createDrawerNavigator, DrawerItems } from "react-navigation-drawer";
import GravarAudio from '../Recorder/GravarAudio';

import Occorrencias from '../Ocorrencias/Occorrencias';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
let LATITUDE = -25.3906745;
let LONGITUDE = -49.1660651;
const LATITUDE_DELTA = 0.0030;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

let latitude_atual = '';
let longitude_atual = '';

let distancia = [];

class GoogleMapScreen extends React.Component {

    static navigationOptions = {
        drawableLabel: 'OpenStreetMap',
        drawerIcon: ({ tintColor }) => (
            <Image
                source={require("../images/Openstreetmap_logo.png")}
                style={{width:40,height:40}}
            />
        )
    };

    constructor(props) {
        super(props);
        this.state = {
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            },
            helpers: [],
            visibleModal: null,
        }
    };

    get mapType() {
        return this.props.provider === PROVIDER_DEFAULT ? MAP_TYPES.STANDARD : MAP_TYPES.HYBRID;
    }

    _getLocationAsync = async () =>{
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if(status !== 'granted') {
            this.setState({
                errorMessage: 'permissao do gps negada',
            });
        }

        let location = await Location.getCurrentPositionAsync();

        navigator.geolocation.getCurrentPosition( position => {
            latitude_atual = position.coords.latitude;
            longitude_atual = position.coords.longitude;

            this.setState({
                marker: {
                    latitude : position.coords.latitude,
                    longitude: position.coords.longitude
                }
            });
        });

        await this.getHelper().then(() => {
            this.renderMarkers();
        });
    };

    componentDidMount() {
        this._getLocationAsync();

    }

    getHelper = async () => {

        const {latitude, longitude} = this.state.region;
        const userLocation = {latitude_atual, longitude_atual};
        const helper = await getHelpers(userLocation);
        this.setState({helpers: helper});

    };

    renderMarkers() {
        const usuario_level = 'agente';

        if (usuario_level === 'super') {
            return this.state.helpers.map((place, i) => (
                <Marker key={i} title={place.properties.nome}
                        coordinate={{latitude: place.geometry.coordinates[1], longitude: place.geometry.coordinates[0]}}
                        description={"Ip do Helper: " + place.properties.ip_helper + " Setor: " + place.properties.setor}
                        onPress={() => console.log(place)}
                >
                    <Image source={require("../images/helper.png")} style={{height: 20, width: 20}} />
                </Marker>
            ))
        } else if (usuario_level === 'agente') {

            distancia = this.state.helpers.map((helper,i ) => {
                let  distancia = {
                    nome: helper.properties.nome,
                    distancia: geolib.getDistance({latitude: latitude_atual, longitude: longitude_atual},
                        {latitude: helper.geometry.coordinates[1], longitude: helper.geometry.coordinates[0]}),
                    latitude: helper.geometry.coordinates[1],
                    longitude: helper.geometry.coordinates[0],
                    ip: helper.properties.ip_helper,
                    setor: helper.properties.setor,
                    check: geolib.isPointWithinRadius({latitude: latitude_atual, longitude: longitude_atual},
                        {latitude:helper.geometry.coordinates[1], longitude: helper.geometry.coordinates[0]},
                        100)
                };
                return distancia;

            });

            if(distancia !== 'undefined' && distancia.length > 0) {
                console.log(JSON.stringify(distancia) + "  os distancia");
                return  distancia.map((helper, i) => {
                    if (helper.check === true) {
                        console.log("danado = " + JSON.stringify(helper.nome + " " + helper.check));
                        return <Marker
                            key={i}
                            title={helper.nome}
                            coordinate={{latitude: helper.latitude, longitude: helper.longitude}}
                            description={"distancia " + helper.distancia + " metros"}
                            onPress={() => console.log(helper)}
                        >
                            <Image source={require("../images/helper.png")} style={{height: 20, width: 20}} />
                        </Marker>

                    }else {
                        return <Marker
                            key={i}
                            title={helper.nome}
                            coordinate={{latitude: helper.latitude, longitude: helper.longitude}}
                            description={ " Ip " + helper.ip }
                        >
                            <Image source={require("../images/helper.png")} style={{height: 20, width: 20}} />
                        </Marker>
                    }
                })
            }
        }

        else if (usuario_level === 'municipio') {

            distancia = this.state.helpers.map((helper,i ) => {
                let  distancia = {
                    nome: helper.properties.nome,
                    distancia: geolib.getDistance({latitude: latitude_atual, longitude: longitude_atual},
                        {latitude: helper.geometry.coordinates[1], longitude: helper.geometry.coordinates[0]}),
                    latitude: helper.geometry.coordinates[1],
                    longitude: helper.geometry.coordinates[0],
                    ip: helper.properties.ip_helper,
                    setor: helper.properties.setor
                };
                return distancia;
            });

            return distancia.map((helper, i) => (
                <Marker key={i}
                        title={helper.nome}
                        coordinate={{latitude: helper.latitude, longitude: helper.longitude}}
                        description={
                            " Distancia: " + helper.distancia}
                        onPress={() => console.log(helper)}
                >
                    <Image source={require("../images/helper.png")} style={{height: 20, width: 20}} />
                </Marker>
            ))
        }
    };

    renderButtonGravarAudio = (text, onPress) => (
        <Button onPress={onPress} style={styles.buttonGravarAudio}>
            <Text style={{ color: 'white'}}>{text}</Text>
        </Button>
    );

    renderButtonFechar = (text, onPress) => (
        <Button onPress={onPress} style={styles.buttonFechar}>
            <Text style={{ color: 'white'}}>{text}</Text>
        </Button>
    );

    modalContentGravarAudio = () => {

        const CustomDrawerContentComponent = (props) => (
            <ScrollView>
                <View style={{height: 80, backgroundColor: "#la8cff", alignItems: 'center', justifyContent: 'center'}} >
                    <Text style={{color: 'white', fontSize: 30 }}>Maps</Text>
                </View>
                <SafeAreaView style={{flex: 1}} forceInset={{ top: 'always', horizontal: 'never'}}>
                    <DrawerItems {...props} />
                </SafeAreaView>
            </ScrollView>
        );

        const MyDrawerNavigator = createDrawerNavigator(
            {
                GravarAudio:GravarAudio,

            },
            {
                contentComponent:CustomDrawerContentComponent
            }
        );

        const GravarAudioJanela = createAppContainer(MyDrawerNavigator);

        return(
                <Container>
                     <GravarAudioJanela>
                     </GravarAudioJanela>
                </Container>
        )
    };

    modalContentOcorrencias() {
        const CustomDrawerContentComponent = (props) => (
            <ScrollView>
                <View style={{height: 80, backgroundColor: "#la8cff", alignItems: 'center', justifyContent: 'center'}} >
                    <Text style={{color: 'white', fontSize: 30 }}>Maps</Text>
                </View>
                <SafeAreaView style={{flex: 1}} forceInset={{ top: 'always', horizontal: 'never'}}>
                    <DrawerItems {...props} />
                </SafeAreaView>
            </ScrollView>
        );

        const MyDrawerNavigator = createDrawerNavigator(
            {
                Occorrencias:Occorrencias,

            },
            {
                contentComponent:CustomDrawerContentComponent
            }
        );

        const OccorrenciasJanela = createAppContainer(MyDrawerNavigator);

        return(
            <Container>
                <OccorrenciasJanela>
                </OccorrenciasJanela>
            </Container>
        )

    }

    renderButtonOccorrencias = (text, onPress) => (
        <Button onPress={onPress} style={styles.buttonOccorrencias}>
            <Text style={{ color: 'white'}}>{text}</Text>
        </Button>
    );

    render() {
        return (
            <Container>
                <Header>
                    <Left style={{ flexDirection: 'row' }}>
                        <Icon onPress={() => this.props.navigation.openDrawer()} name="md-menu" style={{ color: 'white', marginRight: 15 }} />
                    </Left>
                    <View style={{alignItems:'center',justifyContent:'center'}}>
                        <Text style={{ color: 'white' }} >Google Maps</Text>
                    </View>
                    <Right>
                </Right>
                </Header>
                <View>
                    <MapView
                        provider={this.props.provider}
                        style={styles.map}
                        initialRegion={this.state.region}
                        showsUserLocation={true}
                        followsUserLocation={true}
                        showsCompass={true}
                        compassEnabled={true}
                        showsMyLocationButton={true}
                        mapType={this.mapType}
                    >
                        {this.renderMarkers()}
                    </MapView>
                    <View style={{position: 'absolute',
                        bottom: "34%",
                        margin: 10
                    }}>
                        {this.renderButtonGravarAudio('Gravar Audio', ()=> this.setState({visibleModal: 1}))}
                    </View>
                    <View style={{position: 'absolute',
                        bottom: "34%",
                        margin: 10,
                    }}>
                        {this.renderButtonOccorrencias('Occorrencias', ()=> this.setState({visibleModal: 2}))}
                    </View>
                    <Modal
                        visible={this.state.visibleModal === 1}>
                        {this.modalContentGravarAudio()}
                        {this.renderButtonFechar('Fechar', ()=> this.setState({visibleModal: null}))}
                    </Modal>
                    <Modal
                        visible={this.state.visibleModal === 2}>
                        {this.modalContentOcorrencias()}
                        {this.renderButtonFechar('Fechar', ()=> this.setState({visibleModal: null}))}
                    </Modal>
                </View>
            </Container>
        );
    }
}
export default GoogleMapScreen;

const styles = StyleSheet.create({
    map: {
        width: 400,
        height: 800,
    },

    buttonGravarAudio: {
        padding: 10,
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0,0,0,0.1)',

    },

    buttonFechar: {
        padding: 10,
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0,0,0,0.1)',

    },

    buttonOccorrencias: {
        padding: 10,
        margin: 5,
        marginLeft: 232,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0,0,0,0.1)',
    }

});