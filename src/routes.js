import React from 'react';
import {createStackNavigator, NavigationStackOptions} from "react-navigation-stack";
import { createAppContainer} from "react-navigation";
import SignIn from './pages/signIn/index';
import SignUp from './pages/signUp/index';
import Main from './pages/main/index';

const Routes = createStackNavigator({
    SignIn,
    //SignUp,
    Main
});

export default createAppContainer(Routes);
