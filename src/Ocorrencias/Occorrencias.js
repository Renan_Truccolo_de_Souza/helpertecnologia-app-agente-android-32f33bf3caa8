import React from 'react';
import {View, Text, FlatList, SafeAreaView, StyleSheet, ScrollView, TextInput, TouchableHighlight, Alert, AsyncStorage} from 'react-native';
import {Button, Container} from 'native-base';
import IconMaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import {createDrawerNavigator, DrawerItems} from "react-navigation-drawer";
import GravarAudio from "../Recorder/GravarAudio";
import {createAppContainer} from "react-navigation";
import Modal from 'react-native-modal';
import GravarVideo from '../Recorder/video/GravarVideo';
import Axios from 'axios';
import { DocumentPicker } from "expo/build/removed.web";

function createRows(data, columns) {
    const rows = Math.floor(data.length / columns);
    return data;
}

class Occorrencias extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [
                {id: "00", name: "Vandalismo"},
                {id: "01", name: "Pixação"},
                {id: "02", name: "Pau Torando"},
                {id: "03", name: "Violencia contra a Mulher"},
                {id: "04", name: "ZONA"},
                {id: "05", name: "Perturbação do Sossego"}
            ],
            tipoOcorrencia: [ // adicionar sempre de duas em duas opcoes se nao o design quebra
                {id: "00", name: "Destruição de Patrimonio Publico"},
                {id: "01", name: "Pixação"},
                {id: "02", name: "Obsenidade"},
                {id: "03", name: "Difamação"},
            ],
            visibleModal: null,
            color: 'blue',
            selectedItem: null,
            visibleModalAudio: null,
            selectedItemName: null,
            textInputText: null,
        };
    }

    onPressHandler(id, name) {
        this.setState({selectedItem: id});
        this.setState({selectedItemName: name})
    }

    renderButtonRetornar = (text, onPress) => (
        <Button onPress={onPress} style={styles.buttonRetornar}>
            <Text style={{ color: 'white'}}>{text}</Text>
        </Button>
    );

    renderButtonMandar (){
        return (
            <Button style={styles.buttonMandar}
                onPress={() => this.mandar()}>
            <Text style={{ color: 'white'}}>Mandar</Text>
        </Button>
        )
    }



    mandar = async () => {

         let tipoNome = this.state.selectedItemName;
         let detalhes = this.state.textInputText;
         let arquivoAudioUri = await AsyncStorage.getItem('@UriFile:uri');
         let arquivoVideoUri = await AsyncStorage.getItem('@ArquivoVideo:uri');

         let formData = new FormData();

         formData.append("files", {name: 'video.mp4', uri: arquivoVideoUri, type: 'video/mp4'});
         formData.append("files", { name: 'audio.3gp', uri: arquivoAudioUri, type: 'audio/3gp' });
         try {
             let response = await fetch("http://192.168.100.107:4004/", {
                 method: 'post', headers: {
                     'Content-type': 'multipart/form-data',
                 }, body: formData
             });
         }catch (error) {
             console.log('error :' + error);
         }

         this.setState({selectedItem: null});
         this.setState({textInputText: null});
         await AsyncStorage.setItem('@UriFile:uri', '');
         await AsyncStorage.setItem('@ArquivoVideo:uri', '');

         Alert.alert('Enviado com sucesso', 'Sua occorrencia foi enviada para a central(testando)');
         this.setState({ visibleModal: null });

    };

    gravaAudio() {

        const CustomDrawerContentComponent = (props) => (
            <ScrollView>
                <View style={{height: 80, backgroundColor: "#la8cff", alignItems: 'center', justifyContent: 'center'}} >
                    <Text style={{color: 'white', fontSize: 30 }}>Maps</Text>
                </View>
                <SafeAreaView style={{flex: 1}} forceInset={{ top: 'always', horizontal: 'never'}}>
                    <DrawerItems {...props} />
                </SafeAreaView>
            </ScrollView>
        );

        const MyDrawerNavigator = createDrawerNavigator(
            {
                GravarAudio:GravarAudio,

            },
            {
                contentComponent:CustomDrawerContentComponent
            }
        );

        const GravarAudioJanela = createAppContainer(MyDrawerNavigator);

        return(
            <Container>
                <GravarAudioJanela>
                </GravarAudioJanela>
            </Container>
        )

    }

    renderButtonFecharAudio () {

        return (
            <Button style={styles.buttonRetornarAudio}
                    onPress={() => this.setState({visibleModalAudio: null})}>
                <Text style={{ color: 'white'}}>Retornar</Text>
            </Button>
        )
    }

    gravarVideo() {
        const CustomDrawerContentComponent = (props) => (
            <ScrollView>
                <View style={{height: 80, backgroundColor: "#la8cff", alignItems: 'center', justifyContent: 'center'}} >
                    <Text style={{color: 'white', fontSize: 30 }}>Maps</Text>
                </View>
                <SafeAreaView style={{flex: 1}} forceInset={{ top: 'always', horizontal: 'never'}}>
                    <DrawerItems {...props} />
                </SafeAreaView>
            </ScrollView>
        );

        const MyDrawerNavigator = createDrawerNavigator(
            {
                GravarVideo:GravarVideo,

            },
            {
                contentComponent:CustomDrawerContentComponent
            }
        );

        const GravarVideoJanela = createAppContainer(MyDrawerNavigator);

        return(
            <Container>
                <GravarVideoJanela>
                </GravarVideoJanela>
            </Container>
        )
    }

    render() {
        console.log(this.state.visibleModal + " modal ");
        if(this.state.visibleModal === null) {
            return (
                <SafeAreaView style={{backgroundColor: 'grey', flex:1}}>
                    <FlatList data={this.state.data}
                              keyExtractor={item => item.id}
                              renderItem={({item}) => {
                                  return (
                                      <Button style={styles.item}
                                              key={item.id}
                                              onPress={()=> this.setState({visibleModal: item.id})}>
                                          <Text style={{ color: 'white'}}>{item.name}</Text>
                                      </Button>
                                  );
                              }}
                    />
                </SafeAreaView>
            )
        }else if(this.state.visibleModal === "00") {
            console.log(this.state.visibleModal + " else modal");
            const columns = 2;
            return(
               <View style={{backgroundColor: 'grey', flex: 1}}>
                       <View style={{flexDirection: 'row', backgroundColor: 'grey'}}>
                       <Text style={styles.title}>Vandalismo</Text>
                           {this.renderButtonRetornar('Retornar', ()=> this.setState({visibleModal: null}))}
                       </View>
                        <View style={{backgroundColor: 'white'}}>
                          <Text style={styles.subTitle}>Tipo(Opcional)</Text>
                       </View>
                       <SafeAreaView style={{backgroundColor: 'grey'}}>
                           <FlatList data={createRows(this.state.tipoOcorrencia, columns)}
                                     keyExtractor={item => item.id.toString()}
                                     extraData={this.state.selectedItem}
                                     numColumns={columns}
                                     renderItem={({item}) => {
                                         if(item.empty){
                                             return <View style={[styles.item]}/>
                                         }
                                         return (
                                             <TouchableHighlight style={[styles.item, this.state.selectedItem === item.id ? {backgroundColor: 'black'} : {backgroundColor : 'blue'}]}
                                                     onPress={()=> {this.onPressHandler(item.id, item.name)}}>
                                                 <Text style={{ color: 'white'}}>{item.name}</Text>
                                             </TouchableHighlight>
                                         );
                                     }}
                           />
                       </SafeAreaView>
                   <View style={{backgroundColor: 'white'}}>
                       <Text style={styles.subTitle2}>Detalhes(Opcional)</Text>
                   </View>
                   <View style={{backgroundColor: 'white'}}>
                       <TextInput placeholder={'Descreva...'}
                                  style={{height:40, borderColor:'gray', borderWidth: 1, margin: 5}}
                                  multiline={true}
                                  ref={(el) => {this.textInputText = el;}}
                                  onChangeText={(textInputText) => {this.setState({textInputText: textInputText})}}/>
                   </View>
                   <View style={{ flexDirection: 'row', backgroundColor: 'gray'}}>
                       <Button style={styles.buttonGravar}
                               onPress={()=> {this.setState({visibleModalAudio: 'yesAudio'})}}>
                           <Text style={{ color: 'white'}}>Gravar Audio</Text>
                           <IconMaterialCommunity name="record" size={30} color="#900"/>
                       </Button>
                       <Button style={styles.buttonVideo}
                               onPress={()=> {this.setState({visibleModalAudio: 'yesVideo'})}}>
                           <Text style={{ color: 'white'}}>Gravar Video</Text>
                           <IconMaterialCommunity name="camcorder" size={30} color="#900"/>
                       </Button>
                   </View>
                   <View style={{backgroundColor: 'grey'}}>
                       {this.renderButtonMandar()}
                   </View>
                   <View style={{flex:1}}>
                   <Modal visible={this.state.visibleModalAudio === 'yesAudio'}>
                       {this.gravaAudio()}
                       {this.renderButtonFecharAudio()}
                   </Modal>
                       <Modal visible={this.state.visibleModalAudio === 'yesVideo'}>
                           <View style={{flex:1}}>
                               {this.gravarVideo()}
                               {this.renderButtonFecharAudio()}
                           </View>
                       </Modal>
                   </View>
               </View>
            )
        }
        else if(this.state.visibleModal === "01"){
            return (<View style={{flexWrap: 'wrap'}}>
                        <View style={{flexDirection: 'row'}}>
                            <Text>PODE MATAR O FILHO DA PUTA</Text>
                            {this.renderButtonRetornar('Retornar', ()=> this.setState({visibleModal: null}))}
                        </View>
                        <View>
                            {this.renderButtonMandar()}
                        </View>
                    </View>
            )
        }
        else if(this.state.visibleModal === "02"){
            return (<View style={{flexWrap: 'wrap'}}>
                    <View style={{flexDirection: 'row'}}>
                        <Text>TAO COBRANDO INGRESSO PRA VER?</Text>
                        {this.renderButtonRetornar('Retornar', ()=> this.setState({visibleModal: null}))}
                    </View>
                    <View>
                        {this.renderButtonMandar()}
                    </View>
                </View>
            )
        }
        else if(this.state.visibleModal === "03"){
            return (<View style={{flexWrap: 'wrap'}}>
                    <View style={{flexDirection: 'row'}}>
                        <Text>UM TAPINHA NAO DOI, CERTO? JA DIZIA A MUSICA</Text>
                        {this.renderButtonRetornar('Retornar', ()=> this.setState({visibleModal: null}))}
                    </View>
                    <View>
                        {this.renderButtonMandar()}
                    </View>
                </View>
            )
        }
        else if(this.state.visibleModal === "04"){
            return (<View style={{flexWrap: 'wrap'}}>
                    <View style={{flexDirection: 'row'}}>
                        <Text>Onde é, que eu quero ir, custa caro?, as (censurado) sao (censurado)?</Text>
                        {this.renderButtonRetornar('Retornar', ()=> this.setState({visibleModal: null}))}
                    </View>
                    <View>
                        {this.renderButtonMandar()}
                    </View>
                </View>
            )
        }
        else if(this.state.visibleModal === "05"){
            return (<View style={{flexWrap: 'wrap'}}>
                    <View style={{flexDirection: 'row'}}>
                        <Text>FALA PROS LAZARENTOS FICAREM QUETOS, OU VOU SENTAR BALA NELES, ENTENDEU ?</Text>
                        {this.renderButtonRetornar('Retornar', ()=> this.setState({visibleModal: null}))}
                    </View>
                    <View>
                        {this.renderButtonMandar()}
                    </View>
                </View>
            )
        }
        else{
            return (
                <View style={{flexWrap: 'wrap'}}>
                    <View style={{flexDirection: 'row'}}>
                        <Text>fazendo esse nhanho</Text>
                        {this.renderButtonRetornar('Retornar', ()=> this.setState({visibleModal: null}))}
                    </View>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    item: {
        justifyContent: 'center',
        alignItems: 'center',
        flexBasis: 0,
        flexGrow: 1,
        margin: 4,
        padding: 20,
        borderColor: 'rgba(0,0,0,0.1)',
        borderRadius: 4,
    },

    itemOcorrencia:{
        justifyContent: 'center',
        alignItems: 'center',
        flexBasis: 0,
        flexGrow: 1,
        margin: 4,
        padding: 20,
        borderColor: 'rgba(0,0,0,0.1)',
        borderRadius: 4,
    },

    buttonRetornarAudio: {
        padding: 10,
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0,0,0,0.1)',
    },


    buttonMandar: {
        padding: 10,
        margin: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0,0,0,0.1)',
    },

    title: {
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 80,
      fontSize: 30,
    },

    subTitle: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 114,
    },

    subTitle2:{
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 100,
    },

    buttonRetornar: {
        padding: 10,
        margin: 5,
        marginLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0,0,0,0.1)',
    },

    buttonGravar: {
        padding: 10,
        margin: 5,
        marginLeft: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0,0,0,0.1)',
    },

    buttonVideo:{
        padding: 10,
        margin: 5,
        marginRight: 200,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0,0,0,0.1)',

    },

});

export default Occorrencias;