import React from 'react';
import { View, Text, TouchableOpacity, Alert, StyleSheet, AsyncStorage} from 'react-native';
import * as Permissions from "expo-permissions"
import * as MediaLibrary from 'expo-media-library';
import { Camera } from 'expo-camera';

class MyCam extends React.Component{
    state = {
        video: null,
        picture: null,
        recording: false,
        show: true,
    };

saveVideo = async () => {
    const { video } = this.state;
    const asset = await MediaLibrary.createAssetAsync(video.uri);
    await AsyncStorage.setItem("@ArquivoVideo:uri", video.uri);
    console.log(video.uri);
   if(asset) {
       Alert.alert('video salvo');
       this.setState({show: false});

   }
};

stopRecord = async () => {
    this.setState({recording: false}, () => {
        this.cam.stopRecording();
        this.setState({show: true})
    })
};

startRecord = async () => {
    if(this.cam){
        this.setState({recording: true}, async () => {
            const video = await this.cam.recordAsync();
            this.setState({video});
        });
    }
};

toogleRecord = () => {
    const { recording } = this.state;

    if(recording) {
        this.stopRecord();
    }else{
        this.startRecord()
    }
};

render() {
    const { recording, video} = this.state;
    return (
        <Camera
        ref={cam => (this.cam = cam)}
        style={{
            justifyContent: "flex-end",
            alignItems: 'center',
            flex: 1,
            width: "100%"
            }}
        >
            {video && (
                <TouchableOpacity
                    onPress={this.saveVideo}
                    style={this.state.show ? styles.showing : styles.hidden}
                    >
                    <Text style={{ textAlign: 'center'}}>save</Text>
                </TouchableOpacity>
            )}
            <TouchableOpacity
                onPress={this.toogleRecord}
                style={{
                    padding: 20,
                    width: "100%",
                    backgroundColor: recording ? "#ef4f84" : "#4fef97"
                }}
            >
                <Text style={{ textAlign: "center"}}>
                    {recording ? "stop" : "recording"}
                </Text>
            </TouchableOpacity>
        </Camera>
    );
}

}

class GravarVideo extends React.Component {
state = {
    showCamera: false
};

showCamera = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

    if(status === 'granted'){
        this.setState({showCamera: true});
    }
};

render() {
    const { showCamera } = this.state;
    return (
        <View style={{
            justifyContent: "center",
            alignItems: "center",
            flex: 1,
            width: "100%"
            }}
        >
            {showCamera ? (
                <MyCam />
            ) : (
                <TouchableOpacity onPress={this.showCamera}>
                    <Text>Show Camera</Text>
                </TouchableOpacity>
            )}
        </View>
        );
    }
}

export default GravarVideo;

const styles = StyleSheet.create({

    showing: {
        padding: 20,
        width: "100%",
        backgroundColor: "#fff"
    },

    hidden: {
        width: 0,
        height: 0
    }
});